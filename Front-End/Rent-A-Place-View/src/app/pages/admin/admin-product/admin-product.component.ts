import { Component, OnInit } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ProductService } from 'src/app/services/product/product.service';
import { SessionService } from 'src/app/services/storage/session.service';

@Component({
  selector: 'app-admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.scss']
})
export class AdminProductComponent implements OnInit {

  file: any = null;
  formData = new FormData();
  fileName: string | ArrayBuffer | null = "https://banksiafdn.com/wp-content/uploads/2019/10/placeholde-image.jpg"
  features: any;
  type: any;
  id: any = 1;

  constructor(
    private alert: AlertService,
    private product: ProductService,
    private session:SessionService
  ) { }

  ngOnInit(): void {
  }
  getfeature(e: any) {
    this.features = e.target.value;

  }
  getType(e: any) {
    this.type = e.target.value;
  }

  formSubmit(formRef: NgForm) {

    if (formRef.form.status === 'INVALID' || !this.file) {
      this.alert.show({
        title: "Required fields",
        message: "Please fill all required fields",
        icon: 'warning'
      });
      return;
    }
    const values = formRef.form.value;
    console.log("values")
    this.formData.append('name', values?.name);
    this.formData.append('address', values?.address);
    this.formData.append('price', values?.price);
    this.formData.append('type', this.type);
    this.formData.append('features', this.features);
    this.formData.append('userId', this.session.getUsers()?.user?.userId);

    this.product.add(this.formData).subscribe((res: any) => {
      this.fileName = "https://banksiafdn.com/wp-content/uploads/2019/10/placeholde-image.jpg";
      this.file = null;
      formRef.resetForm();
      this.formData.delete('name');
      this.formData.delete('address');
      this.formData.delete('price');
      this.formData.delete('type');
      this.formData.delete('features');

      this.alert.show({
        title: res?.success ? "Success" : "Error",
        message: res?.message,
        icon: res?.success ? 'success' : 'error'
      });
    })

  }

  addProduct() {

  }
  onFileChange(event: any) {
    this.file = event.target.files[0];
    this.fileName = this.file?.name;
    // this.formData.append('banner', "arstu");

    this.formData.append('banner', this.file, this.fileName as string);
    const reader = new FileReader();
    reader.onload = e => this.fileName = reader.result;
    reader.readAsDataURL(this.file);
  }

}
