import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { FeedbackDetailsType, FeedbackService } from 'src/app/services/feedback/feedback.service';
import { SessionService } from 'src/app/services/storage/session.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageReplyComponent } from 'src/app/components/message-reply/message-reply.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  feedbackList: FeedbackDetailsType[] = [];
  messages: any[] = [];
  displayedColumns: string[] = ['message', 'sendByName', 'sendByEmail', 'sentDate', 'reply'];
  allRecordsCount = 0;
  pageSize = 10;
  pageEvent: any;
  constructor(
    private feedback: FeedbackService,
    private api: ApiService,
    private session: SessionService,
    private alert: AlertService,
    public dialog: MatDialog
  ) { }




  ngOnInit(): void {

    this.getMessages();
  }

  onReply(sender: number) {
    const dialogRef = this.dialog.open(MessageReplyComponent, {
      width: '500px',
      data: { senderId: sender, receiverId: this.session.getUsers()?.user?.userId },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  getMessages() {
    console.log(this.session.getUsers());
    const userId = this.session.getUsers()?.user?.userId;
    this.api.get(`/api/messaging/${userId}`).subscribe(((res: any) => {
      this.messages = res?.data?.map((item: any) => ({
        message: item?.message,
        sendByName: item?.sender?.name,
        sendByEmail: item?.sender?.email,
        sendById: item?.sender?.userId,
        sentDate: item?.createdOn,
      }))
    }))
  }

}
